module AssignmentsAndSubmissions where 

import Data.Time.Clock.POSIX
import Data.Time
import System.Directory
import System.FilePath
import Control.Exception
import Control.Monad
import Data.Maybe
import Data.List
import Data.Text (unpack, Text, pack)

-- | Academic year
type Year           = Integer
-- | A user identifier like a username or JMBAG
type UserIdentifier = String
-- | An Assignment type
data Type           = Homework | Exam | Project deriving (Show, Eq)


-- | An Assignment configuration data structure
data Configuration = Configuration
                     { published    :: UTCTime  -- When to publish
                     , deadline     :: UTCTime  -- Submission deadline
                     , lateDeadline :: UTCTime  -- Late submission deadline
                     , files        :: [String] -- File names to expect
                     , minScore     :: Double   -- Minimum achievable
                     , maxScore     :: Double   -- Maximum achievable
                     , required     :: Double   -- Score req to pass
                     } deriving (Show, Read)


data Assignment    = Assignment 
                     { year    :: Year  -- Academic year
                     , atype   :: Type -- Type of assignment
                     , number  :: Int   -- Number of assignment
                     } deriving Show


data Submission    = Submission
                     { userID         :: UserIdentifier -- User identifier
                     , reviewer       :: UserIdentifier -- Reviewer of submission
                     , submittedFiles :: [String]       -- Files submitted
                     , assignment     :: Assignment     -- Submission assignment
                     } deriving Show

-- ====== Testing records ====== --

time = posixSecondsToUTCTime $ fromIntegral 10 -- ovo je bezveze

conf = Configuration time time time ["Homework", "Exercises"] 0 10 5
subm  = Submission "Janko" "Branko" ["Homework"] assign2
subm2 = Submission "Stanko" "Janko" ["Homework", "Exercises"] assign3
subm3 = Submission "Branko" "Stanko" ["Exercises"] assign2
assign = Assignment 2015 Homework 5
assign2 = Assignment 2015 Homework 6
assign3 = Assignment 2016 Project 1


-- ====== Functions ===== --


-- | Lists the user identifier for submissions made for an assignment
listSubmissions :: Assignment -> IO [UserIdentifier]
listSubmissions assign = getFiles $ toPath assign


-- | Views a single submission in detail
getSubmission :: Assignment -> UserIdentifier -> IO Submission
getSubmission assign id = do
   files <- getFiles $ (toPath assign) </> id
   when (files == []) $ error "Submission for this assignment and user does not exist."
   let subFiles = filter (not . isPrefixOf "review") files
   let review = find (isPrefixOf "review") $ files 
   let reviewer = case review of
                   Just a -> drop 1 . takeWhile (/='.') . dropWhile (/='-') $ a 
                   Nothing -> []
   return $ Submission id reviewer subFiles assign 


-- | Creates a new assignment from Assignment, configuration and PDF file
createAssignment :: Assignment -> Configuration -> FilePath -> IO()
createAssignment assign conf fileName = do
 isFile <- doesFileExist $ fileName
 when (not isFile) $ error "Specified file name does not exist."
 let filePath = toPath assign
 createDirectoryIfMissing True filePath
 writeFile (filePath </> ".config") (show conf)
 copyFile fileName (filePath </> "Assignment")
 putStrLn $ "Assignment succesfully created."

-- | Gets the configuration object for an assignment
getConfiguration :: Assignment -> IO Configuration
getConfiguration assign = do
  let filePath = toPath assign
  isDir <- doesDirectoryExist $ filePath 
  when (not isDir) $ error "Configuration file doesn't exist."
  conf <- readFile $ filePath </> ".config"
  return (read conf :: Configuration)


-- | Given a solution body, adds a solution directory/file to the 
-- | directory structure of an assignment.
upload :: Submission -> Text -> String -> IO (Maybe Submission)
upload subm text fileName = do
  isDir <- doesDirectoryExist $ toPath $ assignment subm
  when (not isDir) $ error "Assignment doesn't exist."
  config <- getConfiguration $ assignment subm
  when (fileName `notElem` (files config)) $ error "File name is not permitted!"
  createDirectoryIfMissing False $ getSubmissionPath subm
  writeFile (getSubmissionPath subm </> fileName) (unpack text)
  subm <- getSubmission (assignment subm) (userID subm)
  return $ Just subm


-- | Lists the files contained in a submission
listFiles :: Submission -> IO [FilePath]
listFiles subm = getFiles $ getSubmissionPath subm


-- | Computes file path for a submission
getSubmissionPath :: Submission -> FilePath
getSubmissionPath subm = submPath
 where
   assignPath = toPath $ assignment subm
   submPath = assignPath </> userID subm

-- ====== Helper functions ===== --

-- | Returns file names in given directory if it exists
getFiles :: String -> IO [String]
getFiles filePath = do
   let filterFiles = [".", "..", "Assignment", ".config"]
   isDir <- doesDirectoryExist filePath
   if isDir
     then fmap (filter (`notElem` filterFiles)) (getDirectoryContents filePath)
     else return []


-- | Computes file path for an assignment
toPath :: Assignment -> String
toPath assign = show (year assign) </> show (atype assign) </> show (number assign)




