{-# LANGUAGE TemplateHaskell #-}

module Role where

import Database.Persist (getBy, delete, replace, Entity (..), selectList, entityVal, (==.))
import Database.Persist.Sqlite (runSqlite, runMigrationSilent)
import Database.Persist.TH (mkPersist, mkMigrate, persistLowerCase,
                           share, sqlSettings, derivePersistField)
import Database.Persist.Sql (insert)
import Data.ByteString.Char8 (unpack, pack)
import Data.Hash.MD5

data Role =  Student   Integer
           | TA        Integer Integer
           | Professor
           deriving (Eq,Ord,Show, Read)

derivePersistField "Role"
