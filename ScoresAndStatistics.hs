module ScoresAndStatistics where

import AssignmentsAndSubmissions (Assignment, UserIdentifier, Type, getConfiguration, required, year, atype, Configuration, minScore, maxScore)
import Reviews (RRole (Staff), rrole, reviews, reviewsFor, reviewee, reviewAssignment, score, reviewsForYear, Review, assignment)
import Role
import Control.Monad
import Data.Maybe
import Data.List
import Data.Function

-- | A "bucket" containing a count of values
-- | in a certain range for plotting a histogram.
data Bucket = Bucket {
    rangeMin :: Double
  , rangeMax :: Double
  , count    :: Int
  } deriving Show
              
-- | A statistics container
data Statistics = Statistics { 
    minPossible :: Double
  , maxPossible :: Double
  , mean        :: Double
  , median      :: Double
  , minAchieved :: Double
  , maxAchieved :: Double
  , histogram   :: [Bucket]
  } deriving Show
                  
-- | A user's score and accompanying data
data Score = Score { 
    points :: Double
  , passed :: Bool
  } deriving (Eq, Ord, Show)

data UserScore = UserScore { 
    identifier :: UserIdentifier
  , userScore      :: Score
  } deriving (Eq, Ord, Show)
                
-- | Computes the statistics for an entire academic year
stats :: Integer -> IO Statistics
stats ay = do
  inYear <- reviewsForYear ay
  -- minimum and maximum possible for each assignment
  minsAndMaxs <- forM inYear $ \r -> do
    conf <- getConfiguration $ assignment $ reviewAssignment r
    return (minScore conf, maxScore conf)
  -- minimum and maximum possible for year
  let (minPossible, maxPossible) = (sum $ map fst minsAndMaxs, sum $ map snd minsAndMaxs)
  let byStaff = reviewsByStaff inYear
  let (minAch, maxAch) = (minimum $ scores byStaff, 
                          maximum $ scores byStaff)
  hist <- createHistogram minPossible maxPossible byStaff
  return (Statistics minPossible maxPossible (mean' byStaff) (median' byStaff) minAch maxAch hist)

-- | Computes the statistics for a certain assignment
-- | type in an academic year.
typeStats :: Integer -> Type -> IO Statistics
typeStats ay atp = do
  inYear <- reviewsForYear ay
  minsAndMaxs <- forM inYear $ \r -> do
    conf <- getConfiguration $ assignment $ reviewAssignment r
    return (minScore conf, maxScore conf)
  let (minPossible, maxPossible) = (sum $ map fst minsAndMaxs, sum $ map snd minsAndMaxs)
  let inType = filter (\review -> (atype $ assignment $ reviewAssignment review) == atp) $ reviewsByStaff inYear
  let minAch = minimum $ scores inType
  let maxAch = maximum $ scores inType
  hist <- createHistogram minPossible maxPossible inType
  return (Statistics minPossible maxPossible (mean' inType) 
                    (median' inType) minAch maxAch hist)

mean' :: [Review] -> Double
mean' rs   = (sum xs) / (fromIntegral $ length xs)
  where xs = map score rs
median' :: [Review] -> Double
median' xs | odd len  = head $ drop (len `div` 2) sorted
           | even len = mean'' $ take 2 $ drop index sorted
                  where index = (length sorted `div` 2) - 1
                        sorted = sort $ map score xs
                        mean'' (x:y:[]) = (x + y) / 2.0
                        len = length xs
                       

-- Extract the scores from a list of reviews
scores = map (\review -> score review)

-- Number of buckets is undefined, so we suppose 10 buckets
createHistogram :: Double -> Double -> [Review] -> IO [Bucket]
createHistogram minAch maxAch revs = do
  hist <- forM [0..9] $ \i -> do
    let lower = (fromIntegral i) * minAch / 10.0
    let upper = lower + maxAch / 10.0
    let count = length $ filter (\r -> (score r) > lower && (score r) <= upper) revs
    return (Bucket lower upper count)
  return hist

-- | Computes the statistics for a certain assignment
-- | (combination of AY, type and number)
assignmentStats :: Assignment -> IO Statistics
assignmentStats assign = do
  conf <- getConfiguration assign
  allReviews <- reviews assign
  let byStaff = reviewsByStaff allReviews
  let (minAch, maxAch) = (minimum $ scores byStaff, maximum $ scores byStaff)
  hist <- createHistogram  (minScore conf) (maxScore conf) byStaff
  return (Statistics (minScore conf) (maxScore conf) (mean' allReviews) 
                     (median' allReviews) minAch maxAch hist)

-- | Fetches the user score for an entire academic year
score' :: Integer -> UserIdentifier -> IO Score
score' ay userId = do
  inYear <- reviewsForYear ay
  let forUser = filter (\review -> (reviewee $ reviewAssignment review) == userId) $ reviewsByStaff inYear
  let grade = (sum $ map (\review -> score review) forUser)
  pass <- forM forUser (\x -> return $ isPassable x) >>= sequence
  return (Score grade (and pass))


-- | Fetches the user score for an assignment type in AY
typeScore :: Integer -> Type -> UserIdentifier -> IO Score
typeScore ay atp userId = do
  inYear <- reviewsForYear ay
  let inType = filter (\review -> (atype $ assignment $ reviewAssignment review) == atp) $ reviewsByStaff inYear
  let forUser = filter (\review -> (reviewee $ reviewAssignment review) == userId) inType
  let grade = (sum $ map (\review -> score review) forUser)
  pass <- forM forUser (\x -> return $ isPassable x) >>= sequence
  return (Score grade (and pass))


-- | Fetches the user score for a certain assignment
assignmentScore :: Assignment -> UserIdentifier -> IO Score
assignmentScore asn uid= do
  completedReviews <- reviewsFor asn uid
  let byStaff = reviewsByStaff completedReviews
  let grade = (sum $ map (\review -> score review) byStaff) / (fromIntegral $ length byStaff)
  pass <- forM byStaff (\x -> return $ isPassable x) >>= sequence
  return (Score grade (and pass))


-- | Fetches the ranked users for an entire AY, sorted in
-- | descending order by score.
ranked :: Integer -> IO [UserScore]
ranked ay = do
  inYear <- reviewsForYear ay
  let rankedByScore = rankByScore inYear
  results <- forM rankedByScore $ \x -> do
    pass <- isPassable x
    return (UserScore ((reviewee . reviewAssignment) x) (Score (score x) pass))
  return results


-- | Fetches the ranked users for an assignment type in AY,
-- | sorted in descending order by score.
typeRanked :: Integer -> Type -> IO [UserScore]
typeRanked ay atp = do
  inYear <- reviewsForYear ay
  let inType = filter (\review -> (atype $ assignment $ reviewAssignment review) == atp) $ reviewsByStaff inYear
  let rankedByScore = rankByScore inType
  results <- forM rankedByScore $ \x -> do
    pass <- isPassable x
    return (UserScore ((reviewee . reviewAssignment) x) (Score (score x) pass))
  return results
  

-- | Fetches the ranked users for a specific assignment, sorted
-- | in descending order by score.
assignmentRanked :: Assignment -> IO [UserScore]
assignmentRanked assignment = do
  allReviews <- reviews assignment
  let rankedByScore = rankByScore $ reviewsByStaff allReviews
  results <- forM rankedByScore $ \x -> do
    pass <- isPassable x
    return (UserScore ((reviewee . reviewAssignment) x) (Score (score x) pass))
  return results

-- | Returns whether the user has passed the assignment
isPassable :: Review -> IO Bool
isPassable rev = do
  conf <- getConfiguration $ assignment $ reviewAssignment rev
  return ((score rev) >= (required conf))

rankByScore = sortBy (compare `on` score)
reviewsByStaff = filter (\review -> (rrole $ reviewAssignment review) == Staff)
