{-# LANGUAGE QuasiQuotes, TemplateHaskell, TypeFamilies #-}
{-# LANGUAGE OverloadedStrings, GADTs, FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses, GeneralizedNewtypeDeriving #-}

import Control.Monad
import Database.Persist (getBy, delete, replace, Entity (..), selectList, entityVal, (==.))
import Database.Persist.Sqlite (runSqlite, runMigrationSilent)
import Database.Persist.TH (mkPersist, mkMigrate, persistLowerCase,
                           share, sqlSettings)
import Database.Persist.Sql (insert)
import Data.ByteString.Char8 (unpack, pack)
import Data.Hash.MD5
import Role


type UserIdentifier = String

share [mkPersist sqlSettings, mkMigrate "migrateTables"] [persistLowerCase|
User 
  identifier UserIdentifier
  email      String
  pwdHash    String
  role       Role
  UserIden   identifier
  deriving Show 
 |]

-- | Takes a user identifier, e-mail, password and role.
-- | Performs password hashing and stores the user into the
-- | database, returning a filled User. If creating it fails (e.g.
-- | the user identifier is already taken), throws an appropriate
-- | exception.
createUser :: UserIdentifier -> String -> String -> Role -> IO User
createUser id email pwd role = runSqlite ":database:" $ do
  user <- getBy $ UserIden id
  case user of
    Just _ -> error "User with this identifier already exists."
    Nothing -> do
      let password = md5s . Str $ pwd
      let user = User id email password role
      insert $ user
      return user


-- | Updates a given user. Identifies it by the UserIdentifier (or
-- | maybe database id field, if you added it) in the User and overwrites
-- | the DB entry with the values in the User structure. Throws an
-- | appropriate error if it cannot do so; e.g. the user does not exist.
updateUser :: User -> IO()
updateUser user@(User id email pwd role) = runSqlite ":database:" $ do
  userB <- getBy $ UserIden id
  case userB of
    Nothing -> error "User doesn't exist so it can't be updated."
    Just (Entity userID userB) -> do
      let password = md5s . Str $ pwd
      replace userID $ User id email password role 


-- | Deletes a user referenced by identifier. If no such user or the
-- | operation fails, an appropriate exception is thrown.
deleteUser :: UserIdentifier -> IO()
deleteUser id = runSqlite ":database:" $ do
  user <- getBy $ UserIden id
  case user of 
    Nothing -> error "User with this identifier doesn't exist."
    Just (Entity userID userB) -> delete userID


-- | Lists all the users
listUsers :: IO [User]
listUsers = runSqlite ":database:" $ do
  usersB <- selectList [] []
  let users = map entityVal usersB
  return users


-- | Lists all users in a given role
listUsersInRole :: Role -> IO [User]
listUsersInRole role = runSqlite ":database:" $ do
 users <- selectList [UserRole ==. role] []
 return $ map entityVal users 


-- | Fetches a single user by identifier
getUser :: UserIdentifier -> IO User
getUser id = runSqlite ":database:" $ do
  user <- getBy $ UserIden id
  case user of
    Nothing -> error "User with this identifier doesn't exist."
    Just (Entity userID userB) -> return userB


-- | Checks whether the user has a role of AT LEAST X in a given academic
-- | year.
isRoleInYear :: User -> Role -> Bool
isRoleInYear user role = undefined


-- | Main function for creating a database, should be run first before any other functions  
main = runSqlite ":database:" $ runMigrationSilent migrateTables
